package com.nikosstathis.atmdispenser.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nikosstathis.atmdispenser.error.InsufficientBalanceException;
import com.nikosstathis.atmdispenser.error.NoNoteCombinationException;
import com.nikosstathis.atmdispenser.model.AvailableNote;
import com.nikosstathis.atmdispenser.model.NoteInformation;
import com.nikosstathis.atmdispenser.model.NoteQuantity;
import com.nikosstathis.atmdispenser.model.WithdrawalRequest;
import com.nikosstathis.atmdispenser.service.AtmService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.anyList;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Tests for all the AtmController methods using Mockito and  MockMvc
 */
@RunWith(MockitoJUnitRunner.class)
public class AtmControllerTestCase {

    private MockMvc mvc;

    @InjectMocks
    private AtmController atmController;

    @Mock
    private AtmService atmService;

    @Before
    public void setup() {
        mvc = MockMvcBuilders.standaloneSetup(atmController)
                .setControllerAdvice(new GlobalRestExceptionHandler())
                .build();
    }

    /**
     * tests ATM initialization
     */
    @Test
    public void initializeNotes() throws Exception {
        final NoteInformation expectedInitializedInfo = new NoteInformation(getInitialNoteQuantityList(), 1200, new Date());
        when(atmService.initializeAtm(anyList()))
                .thenReturn(expectedInitializedInfo);

        final String requestBody = new ObjectMapper().writeValueAsString(getInitialNoteQuantityList());

        this.mvc.perform(post("/atmdispenser/rest/initialize").contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(expectedInitializedInfo)));
    }

    /**
     * tests initialization when already initialized
     */
    @Test
    public void initializeForTheSecondTime() throws Exception {
        when(atmService.initializeAtm(anyList()))
                .thenThrow(new IllegalStateException("already initialized"));

        final String requestBody = new ObjectMapper().writeValueAsString(getInitialNoteQuantityList());

        this.mvc.perform(post("/atmdispenser/rest/initialize").contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("error").value("FORBIDDEN"))
                .andExpect(jsonPath("status").value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath("message").value("already initialized"));
    }

    /**
     * tests the request that returns the current note information
     */
    @Test
    public void getNoteInformation() throws Exception {
        final NoteInformation expectedBalanceInfo = new NoteInformation(getInitialNoteQuantityList(), 1200, new Date());
        when(atmService.getNoteInformation())
                .thenReturn(expectedBalanceInfo);

        this.mvc.perform(get("/atmdispenser/rest/noteinformation"))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(expectedBalanceInfo)));
    }

    /**
     * tests the note information without initialization, should return error
     */
    @Test
    public void getNoteInformationBeforeInitialization() throws Exception {
        when(atmService.getNoteInformation())
                .thenThrow(new IllegalStateException("not initialized"));

        this.mvc.perform(get("/atmdispenser/rest/noteinformation"))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("error").value("FORBIDDEN"))
                .andExpect(jsonPath("status").value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath("message").value("not initialized"));
    }

    /**
     * Tests the withdrawals
     */
    @Test
    public void withdraw() throws Exception {
        final List<NoteQuantity> expectedWithdrawalInfo = getInitialNoteQuantityList();
        when(atmService.withdraw(anyLong()))
                .thenReturn(expectedWithdrawalInfo);

        final String requestBody = new ObjectMapper().writeValueAsString(new WithdrawalRequest(1200));

        this.mvc.perform(put("/atmdispenser/rest/withdraw").contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isOk())
                .andExpect(content().string(new ObjectMapper().writeValueAsString(expectedWithdrawalInfo)));
    }

    /**
     * tests the withdrawal without initialization, should return error
     */
    @Test
    public void withdrawBeforeInitialization() throws Exception {
        when(atmService.withdraw(anyLong()))
                .thenThrow(new IllegalStateException("not initialized"));

        final String requestBody = new ObjectMapper().writeValueAsString(new WithdrawalRequest(1200));

        this.mvc.perform(put("/atmdispenser/rest/withdraw").contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("error").value("FORBIDDEN"))
                .andExpect(jsonPath("status").value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath("message").value("not initialized"));
    }

    /**
     * Tests the withdrawal for more money than the existing
     */
    @Test
    public void withdrawMoreThanTheExistingMoney() throws Exception {
        when(atmService.withdraw(anyLong()))
                .thenThrow(new InsufficientBalanceException("Insufficient balance: 100"));

        final String requestBody = new ObjectMapper().writeValueAsString(new WithdrawalRequest(1200));

        this.mvc.perform(put("/atmdispenser/rest/withdraw").contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("error").value("FORBIDDEN"))
                .andExpect(jsonPath("status").value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath("message").value("Insufficient balance: 100"));
    }

    /**
     * Tests the withdrawal for an unavailable combination
     */
    @Test
    public void withdrawForUnavailableCombination() throws Exception {
        when(atmService.withdraw(anyLong()))
                .thenThrow(new NoNoteCombinationException("No note combination available for this amount"));

        final String requestBody = new ObjectMapper().writeValueAsString(new WithdrawalRequest(1200));

        this.mvc.perform(put("/atmdispenser/rest/withdraw").contentType(MediaType.APPLICATION_JSON)
                .content(requestBody))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("error").value("FORBIDDEN"))
                .andExpect(jsonPath("status").value(HttpStatus.FORBIDDEN.value()))
                .andExpect(jsonPath("message").value("No note combination available for this amount"));
    }

    private List<NoteQuantity> getInitialNoteQuantityList() {
        return Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, 10),
                new NoteQuantity(AvailableNote.NOTE_50_EURO, 20))
                .collect(Collectors.toList());
    }
}
