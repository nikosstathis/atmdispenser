package com.nikosstathis.atmdispenser.service;

import com.nikosstathis.atmdispenser.error.InsufficientBalanceException;
import com.nikosstathis.atmdispenser.error.NoNoteCombinationException;
import com.nikosstathis.atmdispenser.model.AvailableNote;
import com.nikosstathis.atmdispenser.model.NoteInformation;
import com.nikosstathis.atmdispenser.model.NoteQuantity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Tests for all the successful and unsuccessful cases of the atm service
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AtmServiceTestCase {

    @Autowired
    private AtmService atmService;

    /**
     * Test that initialization works as expected
     */
    @Test
    @DirtiesContext
    public void initialize() {
        final NoteInformation noteInformation = atmService.initializeAtm(getInitialNoteQuantityList());

        Assert.assertEquals(1200, noteInformation.getTotalCurrentBalance());
        Assert.assertTrue(noteInformation.getCurrentNotes().containsAll(getInitialNoteQuantityList()));
    }


    /**
     * Test that initialization cannot be called twice
     */
    @DirtiesContext
    @Test(expected = IllegalStateException.class)
    public void initializeTwice() {
        atmService.initializeAtm(getInitialNoteQuantityList());
        //the atm has been initialized so if we try to initialize it again, we should get an exception
        atmService.initializeAtm(getInitialNoteQuantityList());
    }

    /**
     * Test that initialization cannot be called for invalid data
     */
    @DirtiesContext
    @Test(expected = IllegalArgumentException.class)
    public void initializeWithInvalidData() {
        atmService.initializeAtm(
                Stream.of(
                        new NoteQuantity(null, 10),
                        new NoteQuantity(AvailableNote.NOTE_20_EURO, 10),
                        new NoteQuantity(AvailableNote.NOTE_50_EURO, 20)
                ).collect(Collectors.toList())
        );
    }

    /**
     * Test that initialization cannot be called for negative amounts
     */
    @DirtiesContext
    @Test(expected = IllegalArgumentException.class)
    public void initializeWithNegativeAmounts() {
        atmService.initializeAtm(
                Stream.of(
                        new NoteQuantity(null, 10),
                        new NoteQuantity(AvailableNote.NOTE_20_EURO, -10),
                        new NoteQuantity(AvailableNote.NOTE_50_EURO, -20)
                ).collect(Collectors.toList())
        );
    }

    /**
     * Test that getNoteInformation works as expected
     */
    @Test
    @DirtiesContext
    public void getNoteInformation() {
        atmService.initializeAtm(getInitialNoteQuantityList());
        final NoteInformation noteInformation = atmService.getNoteInformation();

        Assert.assertEquals(1200, noteInformation.getTotalCurrentBalance());
        Assert.assertTrue(noteInformation.getCurrentNotes().containsAll(getInitialNoteQuantityList()));
    }

    /**
     * Test that getNoteInformation cannot be called before the ATM has been initialized
     */
    @Test(expected = IllegalStateException.class)
    public void getNoteInformationBeforeInitialization() {
        //should throw an exception  because it has not been initialized
        atmService.getNoteInformation();
    }

    /**
     * Test that withdrawal works as expected
     */
    @Test
    @DirtiesContext
    public void withdrawMoney() throws InsufficientBalanceException, NoNoteCombinationException {
        atmService.initializeAtm(getInitialNoteQuantityList());
        List<NoteQuantity> withdrawalResult = atmService.withdraw(580);
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_20_EURO, 4)));
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_50_EURO, 10)));

        withdrawalResult = atmService.withdraw(510);
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_20_EURO, 3)));
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_50_EURO, 9)));

        withdrawalResult = atmService.withdraw(50);
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_50_EURO, 1)));
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_20_EURO, 0)));

        withdrawalResult = atmService.withdraw(20);
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_50_EURO, 0)));
        Assert.assertTrue(withdrawalResult.contains(new NoteQuantity(AvailableNote.NOTE_20_EURO, 1)));
    }

    /**
     * Test that withdrawal works as expected if we withdraw all the money at once
     */
    @Test
    @DirtiesContext
    public void withdrawAllMoneyAtOnce() throws InsufficientBalanceException, NoNoteCombinationException {
        NoteInformation noteInformation = atmService.initializeAtm(getInitialNoteQuantityList());
        final List<NoteQuantity> withdrawalResult = atmService.withdraw(noteInformation.getTotalCurrentBalance());
        Assert.assertTrue(withdrawalResult.containsAll(noteInformation.getCurrentNotes()));

        //now the atm should have no notes
        noteInformation = atmService.getNoteInformation();
        Assert.assertEquals(0, noteInformation.getTotalCurrentBalance());
        Assert.assertTrue(noteInformation.getCurrentNotes().contains(new NoteQuantity(AvailableNote.NOTE_50_EURO, 0)));
        Assert.assertTrue(noteInformation.getCurrentNotes().contains(new NoteQuantity(AvailableNote.NOTE_20_EURO, 0)));
    }

    /**
     * Test that withdrawal cannot work before initialization
     */
    @DirtiesContext
    @Test(expected = IllegalStateException.class)
    public void withdrawMoneyBeforeInitialization() throws InsufficientBalanceException, NoNoteCombinationException {
        atmService.withdraw(580);
    }

    /**
     * Test that we cannot withdraw more money than the existing ones in the ATM
     */
    @DirtiesContext
    @Test(expected = InsufficientBalanceException.class)
    public void withdrawMoreMoneyThanTheExisting() throws InsufficientBalanceException, NoNoteCombinationException {
        atmService.initializeAtm(getInitialNoteQuantityList());
        //we try to withdraw more money than the existing so we should get an exception
        atmService.withdraw(1220);
    }

    /**
     * Test that NoNoteCombinationException is thrown when there is no combination that can serve our amount
     */
    @DirtiesContext
    @Test(expected = NoNoteCombinationException.class)
    public void withdrawMoreMoneyWithNoAvailableNoteCombination() throws InsufficientBalanceException, NoNoteCombinationException {
        atmService.initializeAtm(Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, 3), //we only have 3 20euro notes
                new NoteQuantity(AvailableNote.NOTE_50_EURO, 20)

        ).collect(Collectors.toList()));

        //we try to withdraw 580 euro while having only 3 20notes so this request cannot be processed
        atmService.withdraw(580);
    }

    /**
     * Test that IllegalArgumentException is thrown when the amount is negative
     */
    @DirtiesContext
    @Test(expected = IllegalArgumentException.class)
    public void withdrawNegativeAmount() throws InsufficientBalanceException, NoNoteCombinationException {
        atmService.initializeAtm(Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, 3), //we only have 3 20euro notes
                new NoteQuantity(AvailableNote.NOTE_50_EURO, 20)

        ).collect(Collectors.toList()));

        //we try to withdraw a negative amount so this request cannot be processed
        atmService.withdraw(-580);
    }

    /**
     * Test that IllegalArgumentException is thrown when the amount is negative
     */
    @DirtiesContext
    @Test(expected = IllegalArgumentException.class)
    public void withdrawInvalidAmount() throws InsufficientBalanceException, NoNoteCombinationException {
        atmService.initializeAtm(Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, 3), //we only have 3 20euro notes
                new NoteQuantity(AvailableNote.NOTE_50_EURO, 20)

        ).collect(Collectors.toList()));

        //we try to withdraw cannot be served so this request cannot be processed
        atmService.withdraw(111);
    }

    /**
     * Test that the unsuccessful withdrawals do not affect the current balance and the note quantities
     */
    @Test
    @DirtiesContext
    public void balanceIsNotAffectedAfterUnsuccessfulWithdrawals() {
        NoteInformation initialNoteInformation = atmService.initializeAtm(Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, 3), //we only have 3 20euro notes
                new NoteQuantity(AvailableNote.NOTE_50_EURO, 20)
        ).collect(Collectors.toList()));

        //we try to withdraw 580 euro while having only 3 20notes so this request cannot be processed
        try {
            //this will throw a NoNoteCombinationException, will just catch the exception
            atmService.withdraw(580);
        } catch (InsufficientBalanceException e) {
            //do nothing
        } catch (NoNoteCombinationException e) {
            //do nothing
        }

        //we try to withdraw more money than those initialized in the atm so this request cannot be processed
        try {
            //this will throw an InsufficientBalanceException, will just catch the exception
            atmService.withdraw(initialNoteInformation.getTotalCurrentBalance() + 100);
        } catch (InsufficientBalanceException e) {
            //do nothing
        } catch (NoNoteCombinationException e) {
            //do nothing
        }

        //assert that the balance now has not been modified by the unsuccessful requests
        NoteInformation currentInfo = atmService.getNoteInformation();
        Assert.assertEquals(initialNoteInformation.getTotalCurrentBalance(), currentInfo.getTotalCurrentBalance());
        Assert.assertEquals(initialNoteInformation.getCurrentNotes().size(), currentInfo.getCurrentNotes().size());
        Assert.assertTrue(initialNoteInformation.getCurrentNotes().containsAll(currentInfo.getCurrentNotes()));
    }

    private List<NoteQuantity> getInitialNoteQuantityList() {
        return Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, 10),
                new NoteQuantity(AvailableNote.NOTE_50_EURO, 20)
        ).collect(Collectors.toList());
    }

}
