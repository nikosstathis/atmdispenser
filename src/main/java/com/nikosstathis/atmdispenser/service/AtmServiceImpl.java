package com.nikosstathis.atmdispenser.service;

import com.nikosstathis.atmdispenser.error.InsufficientBalanceException;
import com.nikosstathis.atmdispenser.error.NoNoteCombinationException;
import com.nikosstathis.atmdispenser.model.AvailableNote;
import com.nikosstathis.atmdispenser.model.NoteInformation;
import com.nikosstathis.atmdispenser.model.NoteQuantity;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.LongStream;
import java.util.stream.Stream;

/**
 * AtmServiceImpl is the implementation of the AtmService
 */
@Slf4j
@Service
public class AtmServiceImpl implements AtmService {

    private Map<AvailableNote, Long> noteToQuantity;

    private AtomicLong currentBalance = new AtomicLong(0);

    /**
     * service used to initialize the notes of the ATM
     *
     * @param initialNotes contains the note quantities, how many notes the ATM has been loaded with
     * @return a @{@link NoteInformation} with the current note quantities, the current balance and the time the request was processed
     */
    @Override
    public synchronized NoteInformation initializeAtm(List<NoteQuantity> initialNotes) {
        initialNotes.stream()
                .filter(noteQuantity -> noteQuantity.getNote() == null || noteQuantity.getQuantity() <= 0)
                .findAny()
                .ifPresent(s -> {
                    throw new IllegalArgumentException("the request is invalid");
                });

        if (noteToQuantity != null) {
            throw new IllegalStateException("already initialized");
        }
        noteToQuantity = new ConcurrentHashMap<>();
        setNoteQuantities(initialNotes);
        return getNoteInformation();
    }

    /**
     * service used to retrieve the current note information of the ATM
     *
     * @return a @{@link NoteInformation} with the current note quantities, the current balance and the time the request was processed
     */
    @Override
    public synchronized NoteInformation getNoteInformation() {
        if (noteToQuantity == null) {
            throw new IllegalStateException("not initialized");
        }
        List<NoteQuantity> noteQuantityList = noteToQuantity.entrySet()
                .stream()
                .map(noteToQuantityEntry -> new NoteQuantity(noteToQuantityEntry.getKey(), noteToQuantityEntry.getValue()))
                .collect(Collectors.toList());
        return new NoteInformation(noteQuantityList, currentBalance.get(), new Date());
    }

    /**
     * service used to withdraw money from the ATM
     *
     * @param amount is the amount we want to withdraw
     * @return a list of @{@link NoteQuantity} that shows how many 20notes and how many 50notes were needed
     * @throws InsufficientBalanceException when the requested amount is more that the available amount
     * @throws NoNoteCombinationException   when there is no combination of notes that can return the requested amount
     */
    @Override
    public synchronized List<NoteQuantity> withdraw(final long amount) throws InsufficientBalanceException, NoNoteCombinationException {
        if (amount <= 0 || amount % 10 != 0) {//quick validity check
            throw new IllegalArgumentException("invalid withdrawal amount");
        }

        if (noteToQuantity == null) {
            throw new IllegalStateException("not initialized");
        }

        if (amount > currentBalance.get()) {
            throw new InsufficientBalanceException("Insufficient balance:" + currentBalance.get());
        }

        long max50notesNeeded = amount / 50;
        long max20notesNeeded = amount / 20;

        // We will try to use as many 50 notes as possilbe, no more that max50notesNeeded will be used, so lets create
        // an a reverse stream of the quantities that might be used, and we will check if for each quantity we can have
        // a combination for the requested amount
        Stream<Long> notes50quantityStream = LongStream.range(-1, Math.min(max50notesNeeded, noteToQuantity.get(AvailableNote.NOTE_50_EURO)) + 1)
                .boxed()
                .sorted(Collections.reverseOrder());

        //for each quantity of 50 notes, we test the stream of 20note quantities, so we will need a stream supplier for this
        Supplier<Stream<Long>> notes20QuantityStreamSupplier = () -> LongStream.range(-1, Math.min(max20notesNeeded, noteToQuantity.get(AvailableNote.NOTE_20_EURO)) + 1)
                .boxed();

        final Long foundNotes50quantity = notes50quantityStream
                .filter(notes50quantity -> notes20QuantityStreamSupplier.get()
                        .anyMatch(notes20quantity -> (notes50quantity * 50 + notes20quantity * 20) == amount))
                .findFirst()
                .orElseThrow(() -> new NoNoteCombinationException("No note combination available for this amount"));

        final Long foundNotes20quantity = notes20QuantityStreamSupplier.get()
                .filter(notes20quantity -> (foundNotes50quantity * 50 + notes20quantity * 20) == amount)
                .findFirst()
                .orElseThrow(() -> new NoNoteCombinationException("No note combination available for this amount"));

        //now update the current note quantities and balance
        setNoteQuantities(Stream.of(
                new NoteQuantity(AvailableNote.NOTE_20_EURO, noteToQuantity.get(AvailableNote.NOTE_20_EURO) - foundNotes20quantity),
                new NoteQuantity(AvailableNote.NOTE_50_EURO, noteToQuantity.get(AvailableNote.NOTE_50_EURO) - foundNotes50quantity)
        ).collect(Collectors.toList()));

        return Stream.of(
                new NoteQuantity(AvailableNote.NOTE_50_EURO, foundNotes50quantity),
                new NoteQuantity(AvailableNote.NOTE_20_EURO, foundNotes20quantity)
        ).collect(Collectors.toList());

    }

    /**
     * private method used to set the note quantities and store the current balance. Is is being called on atm
     * initialization and after successful withdrawals
     *
     * @param noteQuantities is a list of @{@link NoteQuantity} containing how many of each note is now available
     */
    private void setNoteQuantities(List<NoteQuantity> noteQuantities) {
        currentBalance.set(0);
        noteQuantities.forEach(noteQuantity -> {
                    log.info("new value for " + noteQuantity.getNote().name() + ":" + noteQuantity.getQuantity());
                    noteToQuantity.put(noteQuantity.getNote(), noteQuantity.getQuantity());
                    currentBalance.set(currentBalance.get() + (noteQuantity.getNote().getValue() * noteQuantity.getQuantity()));
                }
        );
    }

}
