package com.nikosstathis.atmdispenser.service;

import com.nikosstathis.atmdispenser.error.InsufficientBalanceException;
import com.nikosstathis.atmdispenser.error.NoNoteCombinationException;
import com.nikosstathis.atmdispenser.model.NoteInformation;
import com.nikosstathis.atmdispenser.model.NoteQuantity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * AtmService is the inferface defining all the methods that our atm service implements
 */
@Service
public interface AtmService {

    /**
     * service used to initialize the notes of the ATM
     *
     * @param initialNotes contains the note quantities, how many notes the ATM has been loaded with
     * @return a @{@link NoteInformation} with the current note quantities, the current balance and the time the request was processed
     */
    NoteInformation initializeAtm(List<NoteQuantity> initialNotes);


    /**
     * service used to retrieve the current note information of the ATM
     *
     * @return a @{@link NoteInformation} with the current note quantities, the current balance and the time the request was processed
     */
    NoteInformation getNoteInformation();


    /**
     * service used to withdraw money from the ATM
     *
     * @param amount is the amount we want to withdraw
     * @return a list of @{@link NoteQuantity} that shows how many 20notes and how many 50notes were needed
     * @throws InsufficientBalanceException when the requested amount is more that the available amount
     * @throws NoNoteCombinationException   when there is no combination of notes that can return the requested amount
     */
    List<NoteQuantity> withdraw(long amount) throws InsufficientBalanceException, NoNoteCombinationException;

}
