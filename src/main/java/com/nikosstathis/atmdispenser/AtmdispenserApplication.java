package com.nikosstathis.atmdispenser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This is the class defining the entry point of the application
 */
@SpringBootApplication
public class AtmdispenserApplication {

    public static void main(String[] args) {
        SpringApplication.run(AtmdispenserApplication.class, args);
    }
}
