package com.nikosstathis.atmdispenser.rest;

import com.nikosstathis.atmdispenser.error.InsufficientBalanceException;
import com.nikosstathis.atmdispenser.error.NoNoteCombinationException;
import com.nikosstathis.atmdispenser.model.ControllerExceptionError;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.Timestamp;

/**
 * GlobalRestExceptionHandler is used so as to handle all rest exceptions in a uniform  way, responding with a
 *
 * @{@link ControllerExceptionError}
 */
@ControllerAdvice
public class GlobalRestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({IllegalArgumentException.class})
    public ResponseEntity<Object> handleIllegalArgumentException(Exception ex, WebRequest request) {
        return createResponseEntity(HttpStatus.BAD_REQUEST, ex, request);
    }

    @ExceptionHandler({IllegalStateException.class})
    public ResponseEntity<Object> handleIllegalStateException(Exception ex, WebRequest request) {
        return createResponseEntity(HttpStatus.FORBIDDEN, ex, request);
    }

    @ExceptionHandler({InsufficientBalanceException.class})
    public ResponseEntity<Object> handleInsufficientBalanceException(Exception ex, WebRequest request) {
        return createResponseEntity(HttpStatus.FORBIDDEN, ex, request);
    }

    @ExceptionHandler({NoNoteCombinationException.class})
    public ResponseEntity<Object> handleInvalidEntityIdException(Exception ex, WebRequest request) {
        return createResponseEntity(HttpStatus.FORBIDDEN, ex, request);
    }

    private ResponseEntity<Object> createResponseEntity(HttpStatus httpStatus, Exception ex, WebRequest request) {
        return new ResponseEntity<>(
                new ControllerExceptionError(
                        new Timestamp(System.currentTimeMillis()) + "",
                        httpStatus.value(),
                        httpStatus.name(),
                        ex.getMessage(),
                        ((ServletWebRequest) request).getRequest().getServletPath()
                ), new HttpHeaders(), httpStatus
        );
    }

}
