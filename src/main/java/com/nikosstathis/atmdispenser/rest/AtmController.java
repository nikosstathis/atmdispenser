package com.nikosstathis.atmdispenser.rest;

import com.nikosstathis.atmdispenser.error.InsufficientBalanceException;
import com.nikosstathis.atmdispenser.error.NoNoteCombinationException;
import com.nikosstathis.atmdispenser.model.NoteInformation;
import com.nikosstathis.atmdispenser.model.NoteQuantity;
import com.nikosstathis.atmdispenser.model.WithdrawalRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.nikosstathis.atmdispenser.service.AtmService;

import javax.validation.Valid;
import java.util.List;

/**
 * AtmController is the RESTful entry point for all the ATM services
 */
@RestController
@RequestMapping("/atmdispenser/rest/")
public class AtmController {

    @Autowired
    private AtmService atmService;

    /**
     * RESTful service used to initialize the notes of the ATM
     *
     * @param initialNotes contains the note quantities, how many notes the ATM has been loaded with
     * @return a @{@link NoteInformation} with the current note quantities, the current balance and the time the request was processed
     */
    @PostMapping("initialize")
    @CrossOrigin(origins = "http://localhost:4200")
    public NoteInformation initializeAtm(@RequestBody @Valid List<NoteQuantity> initialNotes) {
        return atmService.initializeAtm(initialNotes);
    }

    /**
     * RESTful service used to return the current note information
     *
     * @return a @{@link NoteInformation} with the current note quantities, the current balance and the time the request was processed
     */
    @GetMapping("noteinformation")
    @CrossOrigin(origins = "http://localhost:4200")
    public NoteInformation getCurrentNoteInformation() {
        return atmService.getNoteInformation();
    }


    /**
     * RESTful service used to withdraw money from the ATM
     *
     * @param request is a @{@link WithdrawalRequest} containing the amount we want to withdraw
     * @return a list of @{@link NoteQuantity} that shows how many 20notes and how many 50notes were needed
     * @throws InsufficientBalanceException when the requested amount is more that the available amount
     * @throws NoNoteCombinationException   when there is no combination of notes that can return the requested amount
     */
    @PutMapping("withdraw")
    @CrossOrigin(origins = "http://localhost:4200")
    public List<NoteQuantity> withdraw(@RequestBody @Valid WithdrawalRequest request) throws InsufficientBalanceException, NoNoteCombinationException {
        return atmService.withdraw(request.getAmount());
    }

}
