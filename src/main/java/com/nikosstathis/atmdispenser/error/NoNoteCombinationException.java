package com.nikosstathis.atmdispenser.error;

/**
 * NoNoteCombinationException is the exception defining that there is no combination of notes that can serve the
 * withdrawal request
 */
public class NoNoteCombinationException extends Exception {
    public NoNoteCombinationException(String message) {
        super(message);
    }
}
