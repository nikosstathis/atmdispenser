package com.nikosstathis.atmdispenser.error;

/**
 * InsufficientBalanceException is the exception defining that the requested withdrawal amount is bigger than the
 * balance existing in the ATM so it cannot be served
 */
public class InsufficientBalanceException extends Exception {
    public InsufficientBalanceException(String message) {
        super(message);
    }
}
