package com.nikosstathis.atmdispenser.model;

/**
 * This enumeration indicates the notes our ATM supports
 */
public enum AvailableNote {

    NOTE_20_EURO(20),
    NOTE_50_EURO(50);

    AvailableNote(long value) {
        this.value = value;
    }

    long value;

    public long getValue() {
        return this.value;
    }

}
