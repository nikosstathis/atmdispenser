package com.nikosstathis.atmdispenser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * ControllerExceptionError is used so as to have uniform detailed message on errors we
 * get calling the Restful services providing info on what caused the error.
 * The variable names were taken from the default Spring MVC error messages so that
 * everything is uniform.
 * <p>
 * timestamp returns the time of the incident
 * status is the id of the HTTP status error code
 * error is the description of the HTTP status error code
 * message is a detailed message that may help the used understand what went wrong
 * path is the request path
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ControllerExceptionError {
    String timestamp;
    int status;
    String error;
    String message;
    String path;
}
