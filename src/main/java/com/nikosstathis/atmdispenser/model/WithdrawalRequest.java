package com.nikosstathis.atmdispenser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Positive;

/**
 * WithdrawalRequest is the DTO used  to withdraw money.
 * <p>
 * amount is the requested amount to withdraw
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WithdrawalRequest {
    @Positive
    long amount;
}
