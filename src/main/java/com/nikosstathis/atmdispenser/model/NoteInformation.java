package com.nikosstathis.atmdispenser.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * NoteInformation is used as a response to show the current status of the ATM. It contains a list of
 * NoteQuantity objects indicating how many of each notes exist and also the total current balance of
 * the atm and the timestamp of when the request was processed in UTC
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NoteInformation {
    List<NoteQuantity> currentNotes;
    long totalCurrentBalance;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT")
    Date timestamp;
}
