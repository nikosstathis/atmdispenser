package com.nikosstathis.atmdispenser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

/**
 * NoteQuantity is the DTO used to show how many times a specific note is being used. NoteQuantity is used as an input
 * in the initialization and also as a response in the note information requests and in the withdrawal requests
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NoteQuantity {
    @NotNull
    AvailableNote note;
    @NotNull
    @Positive
    long quantity;
}
