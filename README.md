**1. Introduction**

This is the backend application for an ATM dispenser. It exposes some RESTful services that allow us to initialize the ATM, withdraw money and get the current note information of the ATM.

**2. Architecture**
- This is a Spring Boot application.
- It runs on Java 8 or newer versions of Java
- It is a maven project
- lombok is used to generate methods for all the POJOs
- JUnit is used to test all the services
- Mockito and MockMvc was used for the controller unit tests
- Spring Boot Actuator has been integrated so that the application has some endpoints that help us monitor data, statistics and also the health of the application
- slf4j is used for logging with logback as the implementation
- a logging filter has been used to log all incoming requests

**3. Installation**

go to the application directory  and run:

`mvn clean install`

**4. Running the application**

The application will create a web server listening on port 8081, if this is not suitable for you (e.g. another service is already running on this port) you can modify this by opening the the application.yml file and modifying the server port property:

> server:
> port: 8081
In order to run the application go to the directory of the application and run:

`java -jar ./target/atmdispenser-1.0.0.jar`

or using maven:

`mvn exec:exec`

If everything has gone as expected you should be a message like this in the end of the console logs:

> 12-12-2018 21:13:02.215 ?[35m[main]?[0;39m ?[34mINFO ?[0;39m c.n.a.AtmdispenserApplication.logStarted - Started AtmdispenserApplication in 4.122 seconds (JVM running for 4.823)

To validate that the app is up running, you can check the actuator info page by opening a web brower on this page:

`http://localhost:8081/actuator/info`

where the server should respond this this DTO (with a different timestamp of course ) :

> {"build":{"artifact":"atmdispenser","name":"atmdispenser","time":"2018-12-12T19:11:40.252Z","version":"1.0.0","group":"com.nikosstathis"}}

**5. Testing the application**

There are 2 ways to easily test the application

- using Postman
you can import the file AtmDispenser.postman_collection.json from the resources directory in Postman and then you should be ready to test

- using a small angular app that I created, you can download it from here :
https://gitlab.com/nikosstathis/atm-app.git

**6. Test Cases**

One series of actions that should cover most functionality would be the following:
- Try to get the note information, you should get an error because the ATM was not initialized
- Try to withdraw an amount, you should get an error because the ATM was not initialized
- Initialize the ATM with 2 20euro notes and  10 50euro notes
- Try to initialize the ATM again, you should get an error because the ATM is already initialized
- Get the note information, the response should give you 2 20euro notes, 10 50euro notes and a total balance of 540euro
- Try to withdraw 580euro, you should get an error because the balance is not adequate
- Try to withdraw 460euro, you should get an error because there is no combination that  should give you this amount
- Try to withdraw 440euro, you should have in the response 8 50euro notes, 2 20euro notes
- Get the note information, the response should give you 0 20euro notes and 2 50euro notes
